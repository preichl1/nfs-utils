#!/bin/bash
# set -x # uncomment for debugging
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Description: Test for nfs mount options
#   Author: Bill Peck <bpeck@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2006 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
#. /usr/share/beakerlib/beakerlib.sh || exit 1

#===============================================================================

TESTNAME=${TEST/*\//}
expdir=/exportdir/home
nfsmp=/mnt/$TESTNAME

ORIGINAL_HOSTNAME=$HOSTNAME

rlJournalStart
    rlPhaseStartSetup do-$role-Setup-
    # Load beakerlib you needed
    rlImport kernel/base
    envinfo
	rlFileBackup /etc/sysconfig/nfs /etc/exports
	if echo "$HOSTNAME" | grep -q "localhost"; then
		# In case "localhost.localdomain"
		hostnamectl set-hostname nfs-mount-options-all.test
		HOSTNAME=nfs-mount-options-all.test
	fi
	run 'rm -rf $expdir $nfsmp && mkdir -p $expdir $nfsmp'
	run 'chmod 777 $expdir'
	run 'echo "$expdir  *(rw,sync,no_root_squash)" >/etc/exports'
	run 'service_nfs restart'
    rlPhaseEnd

    rlPhaseStartTest do-$role-Test-fg_bg_retry_devil
	read IP4 _nil <<<$(getDefaultIp)
	run 'iptables -F' -
	MNT_OPTS="vers=3,soft,fg,retry=1"
	log "{Info} Retry 1 minute (60s) NFS mount in *foreground* before giving up."
	run 'iptables -A INPUT -s $IP4 -j DROP' -
	run 'iptables -L' -
	run 'sleep 10'
	start_time=$(date +%s)
	run "mount ${IP4}:$expdir $nfsmp -o ${MNT_OPTS}" 1-255
	retry_time=$((`date +%s` - start_time))
	log "{Info} Actually the nfs.mount hang $retry_time seconds."
	run 'ps aux | grep -v grep | grep ${MNT_OPTS}' 1
	run 'iptables -F' -
	run 'ip6tables -F' -
	run 'mountpoint $nfsmp && umount $nfsmp' 1

	MNT_OPTS="vers=3,soft,bg,retry=1"
	log "{Info} Retry 1 minute (60s) NFS mount in *background* before giving up."
	run 'iptables -A INPUT -s $IP4 -j DROP' -
	run 'iptables -L' -
	run 'sleep 10'
	# When background exit and fork an childprocess, still, returns 0
	run "mount ${IP4}:$expdir $nfsmp -o ${MNT_OPTS}" 0  "should return ok"
	if [ $? -eq 0 ]; then
		# bg ground, the mount fork an child process
		run 'ps aux | grep -v grep | grep ${MNT_OPTS}' 0  "works in bg.. show background process"
		[ $? = 0 ] && {
			log "{Info} We are sleeping a little longer than 60 seconds (retry=1)."
			run "sleep $((retry_time + 10))" 0 "Actually a little more than $retry_time seconds."
			# demo mounting process should exit after 60s as retry=1 (minute)
			run 'ps aux | grep -v grep | grep ${MNT_OPTS}' 1  "retry=1 for 60s should works file after sleep 70s"
		}
	fi
	run 'iptables -F' -
	# should not mount successfully as it expired
	run 'mountpoint $nfsmp && umount $nfsmp' 1
    rlPhaseEnd

    rlPhaseStartTest do-Test-retry-option-should-not-be-ignored
	# test retry of the default value (2 minutes) for foreground - NFS(5)
	run "service_nfs stop"
	run "mount ${HOSTNAME}:$expdir $nfsmp &"
	run "sleep 100 ; service_nfs start ; sleep 10"
	run "nfsstat -m | grep $nfsmp" 0
	[ $? = 0 ] && run "umount $nfsmp"

	# test retry of 10 minutes for foreground
	MNT_OPTS="retry=10"
	log "\n{Info} ${MNT_OPTS}"
	run "service_nfs stop"
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS} &"
	run "sleep 10 ; service_nfs start ; sleep 10"
	run "nfsstat -m | grep $nfsmp" 0
	[ $? = 0 ] && run "umount $nfsmp"

	# test retry of 100 minutes for background
	MNT_OPTS="bg,retry=100"
	log "\n{Info} ${MNT_OPTS}"
	run "service_nfs stop"
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}"
	run "sleep 10 ; service_nfs start ; sleep 10"
	run "nfsstat -m | grep $nfsmp"
	[ $? = 0 ] && run "umount $nfsmp"

	# test retry of the default value (10000 minutes) for background - NFS(5)
	MNT_OPTS="bg"
	log "\n{Info} ${MNT_OPTS}"
	run "service_nfs stop"
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}"
	run "sleep 10 ; service_nfs start ; sleep 10"
	run "nfsstat -m | grep $nfsmp"
	[ $? = 0 ] && run "umount $nfsmp"
    rlPhaseEnd

    rlPhaseStartTest do-$role-Test-resvport_noresvport_test
	run 'service_nfs restart'
	MNT_OPTS="resvport"
	log "{Info} ${MNT_OPTS}"
	run 'mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}' 0
	run 'mountpoint $nfsmp && umount $nfsmp'

	MNT_OPTS="noresvport"
	log "{Info} ${MNT_OPTS}"
	run 'mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}' 1-255  "should got permission denied"
	run 'mountpoint $nfsmp && umount $nfsmp' 1
    rlPhaseEnd

    rlPhaseStartTest do-$role-Test-cache_nocache_test
	MNT_OPTS="noac"
	log "\n{Info} ${MNT_OPTS}"
	run 'chmod 777 $expdir'
	run 'touch $expdir/foo.txt && chmod 777 $expdir/foo.txt'
	run 'service_nfs restart'
	run 'mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}' 0
	[ $? -eq 0 ] && {
		ATTR_TMP1=$(ls -al $nfsmp | grep foo.txt | awk '{ print $1 }')
		chmod 666 $expdir/foo.txt
		sleep 1
		ATTR_TMP2=$(ls -al $nfsmp | grep foo.txt | awk '{ print $1 }')
		log "{Info} $ATTR_TMP1 ~ $ATTR_TMP2"
		run "[ x"$ATTR_TMP1" != x"$ATTR_TMP2" ]" 0  "with noac, attribution should not cached"
	}
	run 'mountpoint $nfsmp && umount $nfsmp'
	run 'rm -f $expdir/foo.txt'

	MNT_OPTS="ac,acregmin=20,acregmax=40,acdirmin=30,acdirmax=60"
	log "\n{Info} ${MNT_OPTS}"
	run 'touch $expdir/foo.txt && chmod 777 $expdir/foo.txt'
	run 'mkdir $expdir/foo.dir && chmod 777 $expdir/foo.dir'
	run 'mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}'
	[ $? -eq 0 ] && {
		ATTR_TMPREG1=$(ls -al $nfsmp | grep foo.txt | awk '{ print $1 }')
		ATTR_TMPDIR1=$(ls -al $nfsmp | grep foo.dir | awk '{ print $1 }')
		chmod 666 $expdir/foo.txt
		chmod 555 $expdir/foo.dir
		sleep 7
		ATTR_TMPREG2=$(ls -al $nfsmp | grep foo.txt | awk '{ print $1 }')
		sleep 7   #7+7
		ATTR_TMPDIR2=$(ls -al $nfsmp | grep foo.dir | awk '{ print $1 }')
		sleep 30  #7+7+30
		ATTR_TMPREG3=$(ls -al $nfsmp | grep foo.txt | awk '{ print $1 }')
		sleep 20  #7+7+30+20
		ATTR_TMPDIR3=$(ls -al $nfsmp | grep foo.dir | awk '{ print $1 }')

		log "{Info} Regfile Attribution: $ATTR_TMPREG1 ~ $ATTR_TMPREG2 ~ $ATTR_TMPREG3"
		run '[ x"$ATTR_TMPREG1" == x"$ATTR_TMPREG2" -a x"$ATTR_TMPREG1" != x"$ATTR_TMPREG3" ]'

		log "{Info} Directory Attribution: $ATTR_TMPDIR1 ~ $ATTR_TMPDIR2 ~ $ATTR_TMPDIR3"
		run '[ x"$ATTR_TMPDIR1" == x"$ATTR_TMPDIR2" -a x"$ATTR_TMPDIR1" != x"$ATTR_TMPDIR3" ]'
	}
	run 'mountpoint $nfsmp && umount $nfsmp'
	run 'rm -f $expdir/foo.txt'
	run 'rm -rf $expdir/foo.dir'

	MNT_OPTS="ac,actimeo=20"
	log "\n{Info} ${MNT_OPTS}"
	run 'mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}'
	[ $? -eq 0 ] && {
		run 'grep ${TESTNAME} /proc/mounts | grep "acregmin=20" | grep "acregmax=20" | grep "acdirmin=20" | grep "acdirmax=20"'
	}
	run 'mountpoint $nfsmp && umount $nfsmp'
    rlPhaseEnd

    rlPhaseStartTest do-$role-Test-sharecache_nosharecache_test
	MNT_OPTS="vers=3,nosharecache"
	MNT_OPTS="nosharecache,ac,acregmin=30,acregmax=60"
	log "\n{Info} ${MNT_OPTS}"
	run 'chmod 777 $expdir'
	run 'touch $expdir/foo.txt && chmod 777 $expdir/foo.txt'
	run 'mkdir -p $nfsmp-{1,2}'
	run 'service_nfs restart'

	run 'mount ${HOSTNAME}:$expdir $nfsmp-1 -o ${MNT_OPTS} &&
	     mount ${HOSTNAME}:$expdir $nfsmp-2 -o ${MNT_OPTS}'
	[ $? = 0 ] && {
		chmod 555  $nfsmp-1/foo.txt
		sleep 1
		chmod 444  $nfsmp-2/foo.txt

		ATTR_TMPREG1=$(ls -al $nfsmp-1 | grep foo.txt | awk '{ print $1 }')
		ATTR_TMPREG2=$(ls -al $nfsmp-2 | grep foo.txt | awk '{ print $1 }')
		ATTR_TMPREG3=$(ls -al $expdir/ | grep foo.txt | awk '{ print $1 }')

		log "{Info} $ATTR_TMPREG1 ~ $ATTR_TMPREG2 ~ $ATTR_TMPREG3"
		run '[ x"$ATTR_TMPREG1" == x"$ATTR_TMPREG2" -a x"$ATTR_TMPREG1" == x"$ATTR_TMPREG3" ]'
	}
	run 'mountpoint $nfsmp-1 && umount $nfsmp-1'
	run 'mountpoint $nfsmp-2 && umount $nfsmp-2'
	run 'rm -f $expdir/foo.txt'
	run 'rm -rf $nfsmp-{1,2}'
    rlPhaseEnd

    rlPhaseStartTest do-$role-Test-lookupcache_test
	MNT_OPTS="ac,acdirmin=30,acdirmax=60,lookupcache=pos"
	log "\n{Info} ${MNT_OPTS}"
	run 'chmod 777 $expdir'
	run 'mkdir -p $expdir/foodir && touch $expdir/foodir/foo-1.txt'
	run 'service_nfs restart'
	run 'mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}'
	[ $? = 0 ] && {
		run 'ls -l $nfsmp/foodir/ | grep foo-1.txt'
		run 'rm -f $expdir/foodir/foo-1.txt'
		run 'sleep 1' -
		log "{Info} Checking again..."
		run 'ls -l $nfsmp/foodir/ | grep foo-1.txt' 1
		run 'umount $nfsmp'
	}
	run 'rm -fr $expdir'
    rlPhaseEnd

    rlPhaseStartTest do-$role-Test-rsize-wsize
	minSize=1024
	minFixSize=4096
	#maxSize=1048576
	run "cat /proc/fs/nfsd/max_block_size" 0 "The default max block size varies depending on RAM size, with a maximum of 1M."
	maxSize=$(cat /proc/fs/nfsd/max_block_size)
	run 'mkdir -p $expdir'
	run 'echo "$expdir  *(rw,sync,no_root_squash)" > /etc/exports'
	run 'service_nfs restart'
	MNT_OPTS="rsize=1024,wsize=1024"
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}"
	run 'grep $nfsmp /proc/mounts' -
	run "cat /proc/mounts | grep -q $MNT_OPTS"
	run 'umount $nfsmp'
	MNT_OPTS="rsize=8192,wsize=8192"
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}"
	run 'grep $nfsmp /proc/mounts' -
	run "cat /proc/mounts | grep -q $MNT_OPTS"
	run 'umount $nfsmp'
	MNT_OPTS="rsize=1,wsize=255" # less than min_size($minSize)
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}"
	run 'grep $nfsmp /proc/mounts' -
	run "cat /proc/mounts | grep -q rsize=$minFixSize,wsize=$minFixSize" 0  "Specified values lower than $minSize are replaced with $minFixSize"
	run 'umount $nfsmp'
	MNT_OPTS="rsize=1048577,wsize=2048576" # great than max_size($maxSize)
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}"
	run 'grep $nfsmp /proc/mounts' -
	run "cat /proc/mounts | grep -q rsize=$maxSize,wsize=$maxSize" 0  "Specified values more than $maxSize are replaced with $maxSize"
	run 'umount $nfsmp'
	MNT_OPTS="rsize=0,wsize=0"
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}"
	run 'grep $nfsmp /proc/mounts' -
	run "cat /proc/mounts | grep -q rsize=$maxSize,wsize=$maxSize" 0  "Specified values more than $maxSize are replaced with $maxSize"
	run 'umount $nfsmp'
	MNT_OPTS="rsize=-1,wsize=-3"
	run "mount ${HOSTNAME}:$expdir $nfsmp -o ${MNT_OPTS}" 1-255
    rlPhaseEnd

for V in $(ls_nfsvers); do
    rlPhaseStartTest do-$role-NFS-mount-options-can-be-lost-when-using-bg-option-vers${V}
	run 'service_nfs stop'
	run 'mount -v -t nfs -o rw,bg,hard,nointr,tcp,vers=$V,timeo=600,rsize=32768,wsize=32768,actimeo=0,noac localhost:$expdir $nfsmp'
	run 'service_nfs start'
	run 'while ! grep -q $nfsmp /proc/mounts; do sleep 15; done' -
	run 'grep $nfsmp /proc/mounts'
	run 'grep $nfsmp /proc/mounts | grep -q "rw,sync"'
	if [ "$V" = "2" ]; then
		run 'grep $nfsmp /proc/mounts | grep -q "vers=$V,rsize=8192,wsize=8192"'
	else
		run 'grep $nfsmp /proc/mounts | grep -q "vers=$V,rsize=32768,wsize=32768"'
	fi
	run 'grep $nfsmp /proc/mounts | grep -q "acregmin=0,acregmax=0,acdirmin=0,acdirmax=0,hard,noac,proto=tcp"'
	run 'grep $nfsmp /proc/mounts | grep -q "timeo=600,retrans=2,sec=sys"'
	run "umount $nfsmp"
    rlPhaseEnd
done

    rlPhaseStartTest do-$role-Test-"mount-nfs-needs-descriptive-error-when-nfsvers-specified-incorrectly"
	run "log=`mktemp -q`" 0 "Creating temporary log file"
	log "{Info} Specify the invalid value."
	for vers in {"v3","vers","+","*",""}; do
		run "mount -t nfs -o vers=$vers localhost:$expdir $nfsmp 2>$log" 32 \
		    "should be failed as expected using invalid value"
		run "mount -t nfs -o nfsvers=$vers localhost:$expdir $nfsmp 2>>$log" 32 \
		    "should be failed as expected using invalid value"
		run "cat $log" -
		run "egrep -q 'Bad nfs mount parameter|parsing error|invalid value' $log"
		run ">$log" 0 "clear $log content"
	done
	log "{Info} Specify the valid number (maybe not supported)."
	run "ls_nfsvers" -
	for vers in {1,2,3,4,5,23}; do
		if ls_nfsvers | grep -q "^$vers"; then
			run "mount -t nfs -o vers=$vers localhost:$expdir $nfsmp"
			run "umount $nfsmp"
			run "mount -t nfs -o nfsvers=$vers localhost:$expdir $nfsmp"
			run "umount $nfsmp"
		else
			run "mount -t nfs -o vers=$vers localhost:$expdir $nfsmp 2>$log" 32 \
			    "should be failed as expected using unsupported version"
			[ $? = 0 ] && run "umount $nfsmp"
			run "mount -t nfs -o nfsvers=$vers localhost:$expdir $nfsmp 2>>$log" 32 \
			    "should be failed as expected using unsupported version"
			[ $? = 0 ] && run "umount $nfsmp"
			run "cat $log" -
			run "egrep -q 'not supported|parsing error|invalid value|call failed' $log"
			run ">$log" 0 "clear $log content"
		fi
	done
	run "rm -f $log" 0 "Remove temporary log file"
    rlPhaseEnd

    rlPhaseStartCleanup do-$role-Cleanup-
	run 'mountpoint $nfsmp && umount $nfsmp' 1
	hostnamectl set-hostname $ORIGINAL_HOSTNAME
	run 'rm -rf $expdir $nfsmp'
	rlFileRestore
	run 'service_nfs restart'
    rlPhaseEnd
rlJournalEnd
#rlJournalPrintText

